# Como utilizar Panoraming

* Descargar en instalar composer 
[Composer](https://getcomposer.org)

* Ejecutar desde la carpeta blog del proyecto el siguiente comando :

> php artisan storage:link

Este comando nos permitira almacenar y visualizar fotos.

* Iniciar el servidor con el siguiente comando:

> php artisan serve

* Ingresar a la aplicacion 

[Panoraming](http://localhost:8000)

* Crear una cuenta nueva o ingresar mediante redes sociales 

* Por defecto al crear una cuenta nueva obtendrá una cuenta de tipo normal, el cual no puede incluir fotos ni url's al crear un evento. Se puede upgradear la cuenta desde la pagina welcome para otener una cuenta premium, y así obtener las funciones antes mencionadas.

* Al crear un evento, este quedará pendiente de aprobación, el cual solo podrá ser aprobado por un administrador, por lo que se recomienda ingresar desde otro navegador o una ventana privada a la aplicacion web con los siguientes datos de ingreso:

> email: panoramingchile@gmail.com

> contraseña : administrador

* Una vez logeado con una cuenta admin, se podrán aprobar los eventos desde la pestaña "Aprobar Eventos"
